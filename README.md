# Instalação

Clone o projeto e abra a linha de comando. Rode a seguinte linha:

```bash
npm install
```

Os módulos serão instalados, e após a conlusão:

```bash
npm run start
```

E os serviços estarão disponíveis na porta 3000.

# OU

Utilize o link:

https://teste-hyppe-clink.herokuapp.com/

para ter acesso as rotas descritas abaixo.

----

# Rotas
----
A seguir se encontram todas as rotas da API, seus métodos e suas funções:

## Usuários

**SIGNUP**

    URL: /signup
    MÉTODOS: POST
    FUNÇÃO: Cadastra um usuário no sistema.

*exemplo de objeto:*

    {
        email: String,
        password: String
    }

**LOGIN**

    URL: /login
    MÉTODOS: POST
    FUNÇÃO: Loga um usuário no sistema, retornando seu token de acesso e sua id para futuras verificações.

*exemplo de objeto:*

    {
        email: String,
        password: String
    }

**GET EVENTS FROM USER**

    URL: /users/:userId/events
    MÉTODOS: GET
    FUNÇÃO: Retorna todos os eventos que o usuário userId participa.

*exemplo de parâmetro:*

    {
        userId: Integer
    }

## Eventos

**GET ALL EVENTS**

    URL: /events
    MÉTODOS: GET
    FUNÇÃO: Retorna todos os eventos. Necessita token de autenticação

*exemplo de header da requisição:*

    {
        x-access-token: Token
    }
 
**GET EVENT**

    URL: /events/:eventId
    MÉTODOS: GET
    FUNÇÃO: Retorna o evento de id eventId, seus participantes, e participantes confirmados. Necessita token de autenticação.

*exemplo de header da requisição:*

    {
        x-access-token: Token
    }

*exemplo de parâmetro:*

    {
        eventId: Integer
    }
    
**CREATE EVENT**

    URL: /events
    MÉTODOS: POST
    FUNÇÃO: Cria um evento. Necessita token de autenticação.

*exemplo de header da requisição:*

    {
        x-access-token: Token
    }

*exemplo de objeto:*

    {
        name: String,
        date: Date (yyyy-MM-dd),
        place: String,
        time: String
    }

**UPDATE EVENT**

    URL: /events/:eventId
    MÉTODOS: PUT
    FUNÇÃO: Atualiza o evento de id eventId. Necessita token de autenticação.

*exemplo de header da requisição:*

    {
        x-access-token: Token
    }

*exemplo de parâmetro:*

    {
        eventId: Integer
    }

*exemplo de objeto:*

    {
        name: String,
        date: Date (yyyy-MM-dd),
        place: String,
        time: String
    }

**DELETE EVENT**

    URL: /events/:eventId
    MÉTODOS: DELETE
    FUNÇÃO: Deleta o evento de id eventId. Necessita token de autenticação.

*exemplo de header da requisição:*

    {
        x-access-token: Token
    }

*exemplo de parâmetro:*

    {
        eventId: Integer
    }


## Participação

**PARTICIPATE EVENT**

    URL: /events/:eventId/participate
    MÉTODOS: POST
    FUNÇÃO: Faz o usuário de id userId participar do evento eventId. Necessita token de autenticação.

*exemplo de header da requisição:*

    {
        x-access-token: Token
    }

*exemplo de parâmetro:*

    {
        eventId: Integer
    }

*exemplo de objeto:*

    {
        userId: Integer
    }

**CONFIRM PARTICIPATION**

    URL: /events/:eventId/confirmParticipation
    MÉTODOS: PUT
    FUNÇÃO: Confirma a presença do usuário no evento eventId. Necessita token de autenticação.

*exemplo de header da requisição:*

    {
        x-access-token: Token
    }

*exemplo de parâmetro:*

    {
        eventId: Integer
    }

*exemplo de objeto:*

    {
        userId: Integer
        check: Boolean
    }    