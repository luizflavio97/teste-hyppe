const { Model, DataTypes } = require('sequelize')

class EventToUser extends Model {
    static init(sequelize) {
        super.init({
            check: DataTypes.BOOLEAN
        }, {
            tableName: 'EventsToUsers',
            sequelize 
        })
    }

    static associate(models) {
        this.belongsTo(models.User, { foreignKey: 'userId', as: 'user' })
        this.belongsTo(models.Event, { foreignKey: 'eventId', as: 'event' })
    }
}

module.exports = EventToUser