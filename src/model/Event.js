const { Model, DataTypes } = require('sequelize')

class Event extends Model {
    static init(sequelize) {
        super.init({
            name: DataTypes.STRING,
            date: DataTypes.DATEONLY,
            time: DataTypes.STRING
        }, {
            tableName: 'Events',
            sequelize 
        })
    }
}

module.exports = Event