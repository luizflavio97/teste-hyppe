const express                   = require('express')
const UserController            = require('./controller/UserController')
const EventController           = require('./controller/EventController')
const EventToUsersController    = require('./controller/EventsToUserController')
const verifyToken               = require('./config/auth')

const routes = express.Router()
/* --- Open Routes --- */
routes.post('/signup', UserController.signup)
routes.post('/login', UserController.login)

/* --- Closed Routes --- */

//User Routes
routes.get('/users/:userId/events', verifyToken, UserController.getEvents)

//Events Routes
routes.get('/events', verifyToken, EventController.getAll)
routes.get('/events/:eventId', verifyToken, EventController.index)
routes.post('/events', verifyToken, EventController.store)
routes.put('/events/:eventId', verifyToken, EventController.update)
routes.delete('/events/:eventId', verifyToken ,EventController.destroy)

//Event Participation Route
routes.post('/events/:eventId/participate', EventToUsersController.store)
routes.put('/events/:eventId/confirmParticipation', EventToUsersController.confirmParticipation)




module.exports = routes