'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Events', 'date',
      {
        type: Sequelize.DATEONLY,
        allowNull: false
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Users', 'email',
      {
        type: Sequelize.DATE,
        allowNull: false
      }
    );
  }
};
