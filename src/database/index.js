const Sequelize = require('sequelize')
const dbConfig = require('../config/database')

const User = require('../model/User')
const Event = require('../model/Event')
const EventToUser = require('../model/EventToUser')

const db = new Sequelize(dbConfig)

User.init(db)
Event.init(db)
EventToUser.init(db)

EventToUser.associate(db.models)

module.exports = db