const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
    let token = req.headers['x-access-token']
    if(!token){
        return res.json({
            auth: false,
            message: 'No token provided'
        })
    }

    jwt.verify(token, process.env.SECRET, function(err, decoded){
        if(err){
            return res.json({
                auth: false,
                message: 'Invalid Token'
            })
        }

        req.userId = decoded.id

        next()
    })

}