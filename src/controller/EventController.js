const User          = require('../model/User')
const Event         = require('../model/Event')
const EventToUser   = require('../model/EventToUser')

module.exports = {
    async getAll (req, res) {
        const events = await Event.findAll({
            group: ['date']
        })

        return res.json(events)
    },

    async index (req, res) {
        const { eventId } = req.params
        let participants = []
        let confirmedParticipants = []

        const event = await Event.findByPk(eventId)

        if(!event){
            return res.json({
                message: 'Event not found'
            })
        }

        const events = await EventToUser.findAll({
            raw: true,
            where: {
                eventId
            }
        })

        for (let i = 0; i < events.length; i++) {
            const user = await User.findOne({
                attributes: ['id', 'email'],
                raw: true,
                where: {
                    id: events[i].userId
                }
            })
            if(events[i].check == 1){
                participants.push(user)
                confirmedParticipants.push(user)
            } else if (events[i].check == 0) {
                participants.push(user)
            }
        }

        return res.json({
            event,
            participants,
            confirmedParticipants
        })
    },

    async store (req, res) {
        const { name, date, place, time } = req.body

        const event = await Event.create({ name, date, place, time })

        return res.json(event)
    },

    async update (req, res) {
        const { eventId } = req.params
        const { name, date, place, time } = req.body

        const event = await Event.findByPk(eventId)

        if(!event){
            res.json({
                error: 'Event not found'
            })
        }

        await Event.update({ name, date, place, time }, {
            where: {
                id: eventId
            }
        })
        
        res.json({
            message: 'Event updated'
        })
    },

    async destroy (req, res) {
        const { eventId } = req.params

        await Event.destroy({
            where: {
                id: eventId
            }
        })

        res.json({
            message: 'Event deleted'
        })
    }
}