const jwt       = require('jsonwebtoken')

const User = require('../model/User')
const Event = require('../model/Event')
const EventToUser   = require('../model/EventToUser')

module.exports = {
    async login (req, res) {
        const { email, password } = req.body

        const user = await User.findOne({
            raw: true,
            where: {
                email,
                password
            }
        })

        if (user) {
            const id = user.id

            let token = jwt.sign({ id }, process.env.SECRET, {
                expiresIn: 86400
            })

            res.json({
                id: user.id,
                email: user.email,
                token
            })
        }
    },

    async signup (req, res) {
        console.log(req.body)
        const { email, password } = req.body

        const userExist = await User.findOne({
            where: {
                email
            }
        })

        if(userExist){
            res.json({
                message: 'Email is already been used.'
            })
        }

        await User.create({ email, password })

        const user = await User.findOne({
            raw: true,
            where: {
                email,
                password
            }
        })

        if (user) {
            const id = user.id

            let token = jwt.sign({ id }, process.env.SECRET, {
                expiresIn: 86400
            })

            res.json({
                id: user.id,
                email: user.email,
                token
            })
        }
    },

    async getEvents (req, res) {
        const { userId } = req.params
        let findEvents = []

        const events = await EventToUser.findAll({
            raw: true,
            where: {
                userId
            }
        })

        for (let i = 0; i < events.length; i++) {
            let event = await Event.findOne({
                group: ['date'],
                raw: true,
                where: {
                    id: events[i].eventId
                }
            })
            await findEvents.push(event)
        }
        
        res.json(findEvents)
    }
}