const User = require('../model/User')
const EventToUser = require('../model/EventToUser')

module.exports = {
    async store (req, res) {
        const { eventId } = req.params
        const { userId } = req.body

        const user = await User.findByPk(userId)

        if(!user){
            res.status(400).json({
                error: 'User not found'
            })
        }

        const userParticipate = await EventToUser.findOne({
            where: {
                userId
            }
        })

        if(userParticipate){
            res.json({
                message: 'This user already participate this event'
            })
        }

        const eventToUser = await EventToUser.create({ check: 0, userId, eventId  })

        return res.json(eventToUser)
    },

    async confirmParticipation (req, res) {
        const { eventId } = req.params
        const { userId, check } = req.body

        const user = await User.findByPk(userId)

        if(!user){
            res.status(400).json({
                error: 'User not found'
            })
        }

        const eventToUser = await EventToUser.update({ check }, {
            where: {
                userId,
                eventId
            }
        })

        return res.json({
            message: 'Participation Confirmed!'
        })
    }
}